#include <stdio.h>
#include <string.h>

/**********************************************
*  Nama        : Iqbal Hadi Nugraha           *
*  NIM         : 220401070409                 *
*  Kelas       : IT204                        *
*  Mata Kuliah : Struktur Data dan Algoritma  *
***********************************************/

struct Data {
    char nama[50];
    char alamat[50];
};

// Fungsi untuk mengurutkan data menggunakan Bubble Sort
void bubbleSort(struct Data arr[], int n) {
    int i, j;
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j].nama, arr[j + 1].nama) > 0) {
                struct Data temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

// Fungsi untuk mengurutkan data menggunakan Selection Sort
void selectionSort(struct Data arr[], int n) {
    int i, j, min_idx;
    for (i = 0; i < n - 1; i++) {
        min_idx = i;
        for (j = i + 1; j < n; j++) {
            if (strcmp(arr[j].nama, arr[min_idx].nama) < 0) {
                min_idx = j;
            }
        }
        if (min_idx != i) {
            struct Data temp = arr[i];
            arr[i] = arr[min_idx];
            arr[min_idx] = temp;
        }
    }
}

int main() {
    struct Data data[] = {
        {"Fahmi", "Jakarta"},
        {"Romi", "Solo"},
        {"Andri", "Jakarta"},
        {"Fadillah", "Banyuwangi"},
        {"Ruli", "Bandung"},
        {"Rudi", "Bali"},
        {"Dendi", "Purwokerto"},
        {"Zaki", "Madiun"}
    };

    int n = sizeof(data) / sizeof(data[0]);

    printf("Data awal:\n");
    for (int i = 0; i < n; i++) {
        printf("%s %s\n", data[i].nama, data[i].alamat);
    }

    // Mengurutkan data menggunakan Bubble Sort
    bubbleSort(data, n);

    printf("\nSetelah diurutkan dengan Bubble Sort:\n");
    for (int i = 0; i < n; i++) {
        printf("%s %s\n", data[i].nama, data[i].alamat);
    }

    // Mengurutkan data menggunakan Selection Sort
    selectionSort(data, n);

    printf("\nSetelah diurutkan dengan Selection Sort:\n");
    for (int i = 0; i < n; i++) {
        printf("%s %s\n", data[i].nama, data[i].alamat);
    }

    return 0;
}
