#include <stdio.h>

/**********************************************
*  Nama        : Iqbal Hadi Nugraha           *
*  NIM         : 220401070409                 *
*  Kelas       : IT204                        *
*  Mata Kuliah : Struktur Data dan Algoritma  *
***********************************************/

// Fungsi untuk binary search
int binarySearch(int arr[], int n, int target) {
    int left = 0;
    int right = n - 1;
    int foundIndexes[n]; // Array untuk menyimpan indeks dari target yang ditemukan
    int count = 0; // Jumlah target yang ditemukan

    while (left <= right) {
        int mid = left + (right - left) / 2;

        if (arr[mid] == target) {
            foundIndexes[count] = mid;
            count++;
            // Cari target di sebelah kiri
            int i = mid - 1;
            while (i >= 0 && arr[i] == target) {
                foundIndexes[count] = i;
                count++;
                i--;
            }
            // Cari target di sebelah kanan
            i = mid + 1;
            while (i < n && arr[i] == target) {
                foundIndexes[count] = i;
                count++;
                i++;
            }
            break;
        }
        else if (arr[mid] < target) {
            left = mid + 1;
        }
        else {
            right = mid - 1;
        }
    }

    if (count == 0) {
        printf("Angka %d tidak ada dalam array\n", target);
    }
    else {
        printf("Angka %d ada di indeks ke ", target);
        for (int i = 0; i < count; i++) {
            printf("%d", foundIndexes[i]);
            if (i < count - 1) {
                printf(" dan ");
            }
        }
        printf("\n");
    }
}

int main() {
    int arr[] = {19, 40, 10, 90, 2, 50, 60, 50, 1};
    int n = sizeof(arr) / sizeof(arr[0]);

    int input1 = 1;
    binarySearch(arr, n, input1);

    int input2 = 50;
    binarySearch(arr, n, input2);

    int input3 = 100;
    binarySearch(arr, n, input3);

    return 0;
}
